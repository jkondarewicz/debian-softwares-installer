#!/bin/bash

FILE=bashrc.original
NEW_FILE=bashrc.path

if [ ! -f "$FILE" ]; then
	cp ~/.bashrc "$FILE"
fi

cat $FILE > $NEW_FILE
echo ". \"$1\"" >> $NEW_FILE
mv $NEW_FILE ~/.bashrc
source ~/.bashrc

