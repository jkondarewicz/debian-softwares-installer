.DEFAULT_GOAL := install.all

PYTHON3_PATH	:= ${HOME}/.bashrc.python3.local

.PHONY := install.all
install.all: install.ansible
	@echo "Installing applications"
	@ansible-playbook ansible/main.yml

.PHONY := install.ansible
install.ansible: install.pip bash.omb
	@echo "Installing ansible"
	@python3 -m pip install --user ansible

.PHONY := bash.path
bash.path: bash.omb
	@echo "export PATH=\"\$$PATH:`python3 -m site --user-base`/bin\"" > "${PYTHON3_PATH}"
	@chmod +x bash.path.sh
	@./bash.path.sh "${PYTHON3_PATH}"

.PHONY := bash.omb
bash.omb:
	@echo "Installing Oh My Bash"
	@if [ ! -d ~/.oh-my-bash ]; then\
 		bash -c "$$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)" --unattended;\
 	fi

.PHONY := install.pip
install.pip: install.required.apps
	@curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
	@python3 get-pip.py
	@rm -rf get-pip.py

.PHONY := install.curl
install.required.apps:
	@sudo apt-get update
	@sudo apt-get install -y curl python3-distutils python3-apt
